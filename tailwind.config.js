module.exports = {
  content: [
    './content/**/*.{njk,md}',
    './_includes/layouts/*.{njk,md}',
    './_includes/*.{njk,md}',
    // Add additional directories here following the same pattern
    // './another-directory/**/*.{njk,md}',
    // './yet-another-directory/**/*.{njk,md}',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Montserrat', 'sans-serif'],
      },
    },
  },
  daisyui: {
    themes: [
      {
        mytheme: {
          "primary": "#b3001b", // Carmine Red: Confident and Professional
          "secondary": "#546e7a", // Slate Gray: Subdued and Trustworthy
          "accent": "#ff7043", // Coral: Energetic and Inviting
          "neutral": "#495057", // Gunmetal: Sophisticated Neutral
          "base-100": "#f5f5f5", // Cultured White: Clean and Modern Background
          "info": "#1e88e5", // Yale Blue: Trustworthy Information
          "success": "#2e7d32", // Japanese Laurel: Stable and Successful
          "warning": "#f9a825", // Selective Yellow: Clear Warning
          "error": "#d32f2f", // Red Pantone: Urgent and Attention-Grabbing
        },
      },
    ],
  },
  plugins: [require('daisyui'), require('@tailwindcss/typography')],
};
