module.exports = {
    title: "Gabe's Tech Insights",
    url: "http://gabrieljperez.com/",
    language: "en",
    description: "Join me, Gabe, as I share my decade-long journey in tech. From full-stack engineering to leading SaaS projects, I bring insights from the worlds of development, design, and tech leadership.",
    author: {
        name: "Gabe Perez",
        email: "gabriel.perez@gatech.edu",
        url: "http://gabrieljperez.com/about-me/"
    }
}
