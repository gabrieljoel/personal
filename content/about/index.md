---
layout: layouts/base.njk
eleventyNavigation:
  key: About Me
  order: 3
---
# About Gabe

## Who I Am
Hello! I'm **Gabe**, a passionate **Full-Stack Engineer**, **Entrepreneur**, and **Tech Leader** with over a decade of experience in the software industry. I blend a strong technical background with a flair for product design and business strategy.

## Education
- **M.S. in Computer Science**
  - _Georgia Tech_
- **B.S. in Computer Engineering**
  - _University of Puerto Rico_

## My Journey
My career path has been a blend of experiences ranging from working with industry giants like **IBM** to diving into the dynamic world of tech startups. This journey has not only honed my development skills but also allowed me to explore and excel in product design.

## Entrepreneurship
As the founder and owner of a successful **Peer Feedback**, I've been trusted by institutions like Georgia Tech for over seven years. This venture has been a testament to my ability to envision, develop, and market tech solutions that resonate with users and stakeholders alike.

## Skill Set
I bring a diverse range of skills to the table, which includes:
- **Tech Development**: Bringing ideas to life with code.
- **UX/UI Design**: Crafting engaging and intuitive user experiences.
- **Sales & Marketing**: Communicating value and connecting with customers.
- **Leadership**: Guiding teams towards success and innovation.

## Looking Ahead
Currently, I am open to **consulting** and **leadership roles**. I thrive in environments that challenge me to drive growth and foster innovative solutions.

## Let's Connect
Whether you're looking for a seasoned tech leader or a dynamic consultant, I'd love to discuss how we can work together. Feel free to [get in touch](gabriel.perez@gatech.edu)!
